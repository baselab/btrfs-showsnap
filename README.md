# btrfs-showsnap
*List all BTRFS snapshots*

Print a list of all BTRFS snapshots along with dates and sizes.

### Dependencies
- sudo
- btrfs-tools

### Usage
Just run it. I'm using it to show snapshots created by snapper.

If you have snapshots created by other software, you'll need to do some tweaks.

