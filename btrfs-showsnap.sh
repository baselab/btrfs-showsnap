#!/bin/bash
# Very dirty script to get btrfs snapshots date and size

filesystems=$( mount | awk '/ type btrfs /{print $3}' )
snapdirR=".snapshots"
snapdirS=${snapdirR//\./\\.}

snapshots=$(\
	for pathR in ${filesystems[@]} ; do
		pathS=${pathR//\./\\.}
		sudo btrfs subv list "${pathR}"/"${snapdirR}" -s -o \
		| awk '{print $NF}' \
		| sed 's/^.*'${snapdirS//\//\\\/}'\//'${pathS//\//\\\/}'\/'${snapdirS//\//\\\/}'\//g ; s/\/\//\//g'
	done )

(for snap in ${snapshots[@]} ; do
	ctime=$( sudo btrfs subvol show "${snap}" | awk '/Creation time/{print $3,$4}' )
	echo -n "${ctime} "
	sudo btrfs qgroup show "${snap}" -f --mbytes \
	| tail -1 \
	| sed 's/^[0-9]*\/[0-9]* /'${snap//\//\\\/}' /'
done) | column -t

